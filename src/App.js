// import logo from './logo.svg';
import './App.css';
import Accordion1 from './components/Accordion1';
import Accordion2 from './components/Accordion2';

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <div>
        <Accordion1 />
        <Accordion2 />
    </div>
  );
}

export default App;
