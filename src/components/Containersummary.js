import * as React from 'react';
import Container from '@mui/material/Container';
import Accordion1 from './Accordion1';
import Accordion2 from './Accordion2';

export default function Containersummary() {
    return (
        <div>
            <Box sx={{ width: 300, height: 300 }}>
                <Container maxWidth="sm">
                    <Accordion1 />
                    <Accordion2 />
                </Container>
            </Box>
        </div>
    );
}